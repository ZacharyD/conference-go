from common.json import ModelEncoder
from .models import Location, Conference


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
    ]

    def get_extra_data(self, o):  # o is just a local variable
        return {"state": o.state.abbreviation}


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "starts",
        "ends",
        "created",
        "updated",
        "max_presentations",
        "max_attendees",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }
